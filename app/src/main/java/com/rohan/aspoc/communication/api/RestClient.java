package com.rohan.aspoc.communication.api;

import com.google.gson.Gson;
import com.rohan.aspoc.application.Constants;
import com.rohan.aspoc.communication.api.interfaces.IProducts;
import com.rohan.aspoc.models.responsedtos.ProductsResponseDTO;
import com.rohan.aspoc.utils.GsonUtils;
import com.squareup.okhttp.OkHttpClient;

import java.util.concurrent.TimeUnit;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.client.OkClient;
import retrofit.converter.GsonConverter;

/**
 * Created by rohan on 11/3/16.
 */
public class RestClient implements IProducts {
    private static final String URL = Constants.BASE_URL;

    private static RestClient mRestClient;
    private static RestAdapter mRestAdapter;

    public static RestClient getClient(){
        if(mRestClient == null)
            mRestClient = new RestClient();
        return mRestClient;
    }

    Gson gson = GsonUtils.getDateCompatibleGson();

    private RestClient(){
        final OkHttpClient okHttpClient = new OkHttpClient();
        okHttpClient.setReadTimeout(Constants.CONNECTION_TIMEOUT, TimeUnit.SECONDS);
        okHttpClient.setConnectTimeout(Constants.CONNECTION_TIMEOUT, TimeUnit.SECONDS);

        mRestAdapter = new RestAdapter.Builder()
                .setEndpoint(URL)
                .setClient(new OkClient(okHttpClient))
                .setConverter(new GsonConverter(gson))
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .build();
    }

    /**
     * linking of interface to rest-client
     */

    @Override
    public void getProducts(Callback<ProductsResponseDTO> callback) {
        IProducts api = mRestAdapter.create(IProducts.class);
        api.getProducts(callback);
    }

    @Override
    public void getFeaturedProducts(Callback<ProductsResponseDTO> callback) {
        IProducts api = mRestAdapter.create(IProducts.class);
        api.getFeaturedProducts(callback);
    }
}
