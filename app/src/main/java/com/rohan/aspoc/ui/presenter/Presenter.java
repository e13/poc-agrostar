package com.rohan.aspoc.ui.presenter;

/**
 * Created by rohan on 24/3/16.
 */
public interface Presenter<T> {

    void attachView(T view);

    void detachView();

}
