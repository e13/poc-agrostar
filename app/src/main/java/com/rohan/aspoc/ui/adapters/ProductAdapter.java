package com.rohan.aspoc.ui.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.rohan.aspoc.R;
import com.rohan.aspoc.models.Product;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by rohan on 24/3/16.
 */
public class ProductAdapter extends RecyclerView.Adapter<ProductAdapter.ProductViewHolder> implements Filterable{

    private List<Product> products;
    private Callback callback;

    private List<Product> mDataSetBackup = new ArrayList<>();
    private List<Product> filteredList;

    public ProductAdapter() {
        this.products = Collections.emptyList();
        this.filteredList = new ArrayList<>();
    }

    public ProductAdapter(List<Product> products) {
        this.products = products;
    }

    public void setProducts(List<Product> products) {
        this.products = products;
        this.filteredList = new ArrayList<>();
    }

    public void setCallback(Callback callback) {
        this.callback = callback;
    }

    @Override
    public ProductViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_product, parent, false);
        final ProductViewHolder viewHolder = new ProductViewHolder(itemView);
        viewHolder.llContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (callback != null) {
                    callback.onItemClick(viewHolder.product);
                }
            }
        });
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ProductViewHolder holder, int position) {
        Product product = products.get(position);
        Context context = holder.tvProductName.getContext();
        holder.product = product;
        holder.tvProductName.setText(product.getName());
        holder.tvProductDescription.setText(product.getDescription());
        holder.tvProductBrand.setText(
                context.getResources().getString(R.string.text_product_brand, product.getBrandName()));
        holder.tvProductPrice.setText(
                context.getResources().getString(R.string.text_product_price, product.getPrice()));
        Picasso.with(context).load(product.getImageUrl()).placeholder(R.drawable.icon_launcher).into(holder.civProductImage);
    }

    @Override
    public int getItemCount() {
        return products.size();
    }

    @Override
    public Filter getFilter() {
        return new CustomFilter(this,products);
    }

    public int getFilteredSize(){
        return filteredList.size();
    }
    public interface Callback {
        void onItemClick(Product product);
    }

    public class ProductViewHolder extends RecyclerView.ViewHolder {
        CircleImageView civProductImage;
        TextView tvProductName;
        TextView tvProductDescription;
        TextView tvProductBrand;
        TextView tvProductPrice;
        LinearLayout llContainer;

        Product product;

        public ProductViewHolder(View itemView) {
            super(itemView);
            llContainer = (LinearLayout) itemView.findViewById(R.id.container_product);
            tvProductName = (TextView) itemView.findViewById(R.id.tv_product_name);
            tvProductDescription = (TextView) itemView.findViewById(R.id.tv_product_description);
            tvProductBrand = (TextView) itemView.findViewById(R.id.tv_product_brand);
            tvProductPrice = (TextView) itemView.findViewById(R.id.tv_product_price);
            civProductImage = (CircleImageView) itemView.findViewById(R.id.civ_product_image);
        }
    }

    /*Filterable*/
    private class CustomFilter extends Filter {
        private final ProductAdapter adapter;
        private final List<Product> originalList;
        private final List<Product> filteredList;

        public CustomFilter(ProductAdapter adapter, List<Product> products) {
            super();
            this.adapter = adapter;
            this.originalList = new LinkedList<>(products);
            this.filteredList = new ArrayList<>();
        }

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            filteredList.clear();
            final FilterResults results = new FilterResults();

            if (constraint.length() == 0 || constraint.toString().equals("")) {
                filteredList.addAll(mDataSetBackup);
            } else {
                final String filterPattern = constraint.toString().toLowerCase().trim();

                for (final Product product : originalList) {
                    boolean hasName = product.getName().toLowerCase().contains(filterPattern);
                    boolean hasBrand = product.getBrandName().toLowerCase().contains(filterPattern);
                    boolean hasDescription = product.getDescription().toLowerCase().contains(filterPattern);
                    if ( hasName || hasBrand || hasDescription) {
                        filteredList.add(product);
                    }
                }
            }
            results.values = filteredList;
            results.count = filteredList.size();
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            adapter.filteredList.clear();
            adapter.filteredList.addAll((ArrayList<Product>) results.values);
            adapter.setFilteredDataIntoView();
            adapter.notifyDataSetChanged();
        }
    }

    private void setFilteredDataIntoView() {
        products.clear();
        products.addAll(filteredList);
    }
}
