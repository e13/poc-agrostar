package com.rohan.aspoc.ui.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.rohan.aspoc.R;
import com.rohan.aspoc.application.Constants;
import com.rohan.aspoc.models.Product;
import com.rohan.aspoc.ui.adapters.ProductAdapter;
import com.rohan.aspoc.utils.FontUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by rohan on 24/3/16.
 */
public class SearchActivity extends BaseActivity {

    private String query;
    private List<Product> products;
    private Toolbar mToolbar;
    private RecyclerView rvProducts;
    private ProgressBar progressBar;
    private TextView tvDefaultMessage;

    public static Intent newIntent(Context context, String query, List<Product> products) {
        Intent intent = new Intent(context, SearchActivity.class);
        intent.putExtra(Constants.BUNDLE_KEYS.EXTRA_SEARCH_QUERY, query);
        intent.putParcelableArrayListExtra(Constants.BUNDLE_KEYS.EXTRA_PRODUCT_LIST, new ArrayList<Product>(products));
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        FontUtils.init(this);
        init();
    }

    private void init() {
        initData();
        initToolbar();
        initPlaceholders();
        initRecylerView();
    }

    private void initPlaceholders() {
        progressBar = (ProgressBar) findViewById(R.id.progress);
        tvDefaultMessage = (TextView) findViewById(R.id.tv_default);
    }

    private void initToolbar() {
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
        setTitle(getString(R.string.title_search));
    }

    private void initRecylerView() {
        rvProducts = (RecyclerView) findViewById(R.id.rv_products);
        setupRecyclerView(rvProducts);
        initializeResults();
    }

    private void initializeResults() {
        ProductAdapter adapter = (ProductAdapter) rvProducts.getAdapter();
        adapter.setProducts(products);
        adapter.notifyDataSetChanged();
        rvProducts.requestFocus();
//        hideSoftKeyboard();
        progressBar.setVisibility(View.INVISIBLE);
        tvDefaultMessage.setVisibility(View.INVISIBLE);
        rvProducts.setVisibility(View.VISIBLE);

        adapter.getFilter().filter(query);
    }

    private void showDefaultState() {
        rvProducts.setVisibility(View.GONE);
        tvDefaultMessage.setVisibility(View.VISIBLE);
    }

    private void initData() {
        query = getIntent().getStringExtra(Constants.BUNDLE_KEYS.EXTRA_SEARCH_QUERY);
        products = (List<Product>)getIntent().getExtras().get(Constants.BUNDLE_KEYS.EXTRA_PRODUCT_LIST);
    }

    public void setupRecyclerView(RecyclerView rvProducts) {
        ProductAdapter adapter = new ProductAdapter();
        adapter.setCallback(new ProductAdapter.Callback() {
            @Override
            public void onItemClick(Product product) {
                startActivity(ProductDetailsActivity.newIntent(SearchActivity.this, product));
            }
        });
        rvProducts.setAdapter(adapter);
        rvProducts.setLayoutManager(new LinearLayoutManager(this));
    }
}
