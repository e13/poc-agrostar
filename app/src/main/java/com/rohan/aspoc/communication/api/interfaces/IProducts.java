package com.rohan.aspoc.communication.api.interfaces;

import com.rohan.aspoc.application.Constants;
import com.rohan.aspoc.models.responsedtos.ProductsResponseDTO;

import retrofit.Callback;
import retrofit.http.GET;

/**
 * Created by rohan on 12/3/16.
 */
public interface IProducts {

    @GET(Constants.REST_API.GET_PRODUCTS)
    void getProducts(Callback<ProductsResponseDTO> callback);

    @GET(Constants.REST_API.GET_FEATURED_PRODUCTS)
    void getFeaturedProducts(Callback<ProductsResponseDTO> callback);
}
