package com.rohan.aspoc.communication.manager;

import android.content.Context;

import com.rohan.aspoc.communication.api.RestClient;
import com.rohan.aspoc.communication.manager.events.FetchedFeaturedProductsEvent;
import com.rohan.aspoc.communication.manager.events.FetchedProductsEvent;
import com.rohan.aspoc.communication.manager.events.GetFeaturedProductsEvent;
import com.rohan.aspoc.communication.manager.events.GetProductsEvent;
import com.rohan.aspoc.models.responsedtos.ProductsResponseDTO;
import com.rohan.aspoc.utils.Logger;
import com.rohan.aspoc.utils.MockUtils;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by rohan on 11/3/16.
 */
public class EventManager {
    private Context mContext;
    private Bus mBus;
    private RestClient mRestClient;

    public EventManager(Context mContext, Bus mBus) {
        this.mContext = mContext;
        this.mBus = mBus;
        this.mRestClient = RestClient.getClient();
    }

    /**
     * listening to events
     */

    @Subscribe
    public void onGetProductsEvent(GetProductsEvent event){
        Callback<ProductsResponseDTO> callback = new Callback<ProductsResponseDTO>() {

            @Override
            public void success(ProductsResponseDTO restResponse, Response response) {
                Logger.logInfo("response:" + response.toString());
                Logger.logInfo("list of products:" + restResponse.toString());
                mBus.post(new FetchedProductsEvent(restResponse.isStatus(),restResponse.getMessage(),restResponse.getData()));
            }

            @Override
            public void failure(RetrofitError error) {
                String errorMessage = error.getLocalizedMessage();
                //mBus.post(new FetchedProductsEvent(false, errorMessage, null));

                /**
                 *
                 * TODO/FYI
                 * because i don't have any rest apis available..
                 * i cannot show off my comm framework,
                 * so all api calls will fail and thus im sending back dummy data for population
                 * during the failure callbacks
                 *
                 */
                mBus.post(new FetchedProductsEvent(true, errorMessage, MockUtils.getProducts()));
            }
        };
        mRestClient.getProducts(callback);
    }

    @Subscribe
    public void onGetFeaturedProductsEvent(GetFeaturedProductsEvent event){
        Callback<ProductsResponseDTO> callback = new Callback<ProductsResponseDTO>() {

            @Override
            public void success(ProductsResponseDTO restResponse, Response response) {
                Logger.logInfo("response:" + response.toString());
                Logger.logInfo("list of  featured products:" + restResponse.toString());
                mBus.post(new FetchedFeaturedProductsEvent(restResponse.isStatus(),restResponse.getMessage(),restResponse.getData()));
            }

            @Override
            public void failure(RetrofitError error) {
                String errorMessage = error.getLocalizedMessage();
                //mBus.post(new FetchedProductsEvent(false, errorMessage, null));

                /**
                 *
                 * TODO/FYI
                 * because i don't have any rest apis available..
                 * i cannot show off my comm framework,
                 * so all api calls will fail and thus im sending back dummy data for population
                 * during the failure callbacks
                 *
                 */
                mBus.post(new FetchedFeaturedProductsEvent(true, errorMessage, MockUtils.getFeaturedProducts()));
            }
        };
        mRestClient.getProducts(callback);
    }
}
