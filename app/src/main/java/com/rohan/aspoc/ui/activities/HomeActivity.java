package com.rohan.aspoc.ui.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderView;
import com.rohan.aspoc.R;
import com.rohan.aspoc.models.Product;
import com.rohan.aspoc.ui.adapters.ProductAdapter;
import com.rohan.aspoc.ui.presenter.HomeMvpView;
import com.rohan.aspoc.ui.presenter.impls.HomePresenter;
import com.rohan.aspoc.utils.FontUtils;
import com.rohan.aspoc.utils.LanguageUtils;

import java.util.List;

/**
 * Created by rohan on 24/3/16.
 */
public class HomeActivity extends BaseActivity implements HomeMvpView {

    private HomePresenter mPresenter;
    private Toolbar toolbar;
    private RecyclerView rvProducts;
    private ProgressBar progressBar;
    private TextView tvDefaultMessage;
    private SliderLayout sliderFeaturedProducts;

    public static Intent newIntent(Context context) {
        Intent intent = new Intent(context, HomeActivity.class);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initPresenter();
        setContentView(R.layout.activity_home);
        FontUtils.init(this);
        initViews();

        mPresenter.loadProducts();
        mPresenter.loadFeaturedProducts();
    }

    private void initViews() {
        initToolbar();
        initPlaceholders();
        initRecylerView();
        initFeaturedProductsSlider();
    }

    private void initFeaturedProductsSlider() {
        sliderFeaturedProducts = (SliderLayout) findViewById(R.id.slider);
    }

    private void initPlaceholders() {
        progressBar = (ProgressBar) findViewById(R.id.progress);
        tvDefaultMessage = (TextView) findViewById(R.id.tv_default);
    }

    private void initRecylerView() {
        rvProducts = (RecyclerView) findViewById(R.id.rv_products);
        setupRecyclerView(rvProducts);
    }

    private void initToolbar() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
    }

    private void initPresenter() {
        mPresenter = new HomePresenter();
        mPresenter.attachView(this);
    }

    @Override
    protected void onDestroy() {
        mPresenter.detachView();
        super.onDestroy();
    }

    @Override
    protected void onStop() {
        sliderFeaturedProducts.stopAutoCycle();
        super.onStop();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menus, menu);

        initSearchMenu(menu);

        return super.onCreateOptionsMenu(menu);
    }

    private void initSearchMenu(Menu menu) {
        MenuItem searchItem = menu.findItem(R.id.action_search);
        SearchView searchView =
                (SearchView) MenuItemCompat.getActionView(searchItem);
        searchView.setSubmitButtonEnabled(true);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                startSearchActivity(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });
    }

    private void startSearchActivity(String query) {
        //will start an activity showing list of products as matched by query string [products are already fetched ie the 12 Mock data]
        startActivity(SearchActivity.newIntent(HomeActivity.this,query,mPresenter.getProducts()));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_exit:
                finish();
                break;
            case R.id.action_language:
                changeLocale();
                break;
            case R.id.action_about:
                toastCustom(getString(R.string.shameless_promotion), Gravity.CENTER, Toast.LENGTH_LONG);
                break;
            //no default needed.
        }
        return super.onOptionsItemSelected(item);
    }

    private void changeLocale() {
        LanguageUtils.init(this);
    }

    /**
     * ===============Presenter methods START=================
     */

    @Override
    public Context getContext() {
        return this;
    }

    @Override
    public void showProducts(List<Product> products) {
        ProductAdapter adapter = (ProductAdapter) rvProducts.getAdapter();
        adapter.setProducts(products);
        adapter.notifyDataSetChanged();
        rvProducts.requestFocus();
//        hideSoftKeyboard();
        progressBar.setVisibility(View.INVISIBLE);
        tvDefaultMessage.setVisibility(View.INVISIBLE);
        rvProducts.setVisibility(View.VISIBLE);
    }

    @Override
    public void showFeaturedProducts(List<Product> products) {
        TextSliderView textSliderView = null;
        for (final Product product : products) {
            textSliderView = new TextSliderView(this);
            textSliderView
                    .description(product.getBrandName())
                    .image(product.getImageUrl());
            textSliderView.setOnSliderClickListener(new BaseSliderView.OnSliderClickListener() {
                @Override
                public void onSliderClick(BaseSliderView slider) {
                    showFeaturedProductDetail(product);
                }
            });
            sliderFeaturedProducts.addSlider(textSliderView);
        }

    }

    private void showFeaturedProductDetail(Product product) {
        startActivity(ProductDetailsActivity.newIntent(HomeActivity.this, product));
    }

    @Override
    public void showMessage(int id) {
        progressBar.setVisibility(View.INVISIBLE);
        tvDefaultMessage.setVisibility(View.VISIBLE);
        rvProducts.setVisibility(View.INVISIBLE);
        tvDefaultMessage.setText(getString(id));
    }

    @Override
    public void showProgressIndicator() {
        progressBar.setVisibility(View.VISIBLE);
        tvDefaultMessage.setVisibility(View.INVISIBLE);
        rvProducts.setVisibility(View.INVISIBLE);
    }


    public void setupRecyclerView(RecyclerView rvProducts) {
        ProductAdapter adapter = new ProductAdapter();
        adapter.setCallback(new ProductAdapter.Callback() {
            @Override
            public void onItemClick(Product product) {
                startActivity(ProductDetailsActivity.newIntent(HomeActivity.this, product));
            }
        });
        rvProducts.setAdapter(adapter);
        rvProducts.setLayoutManager(new LinearLayoutManager(this));
    }
}
