package com.rohan.aspoc.communication.manager.events;

import com.rohan.aspoc.models.Product;

import java.util.List;

/**
 * Created by rohan on 24/3/16.
 */
public class FetchedProductsEvent {
    private boolean status;
    private String message;
    private List<Product> data;

    public FetchedProductsEvent(boolean status, String message, List<Product> data) {
        this.status = status;
        this.message = message;
        this.data = data;
    }

    public boolean isStatus() {
        return status;
    }

    public String getMessage() {
        return message;
    }

    public List<Product> getData() {
        return data;
    }
}
