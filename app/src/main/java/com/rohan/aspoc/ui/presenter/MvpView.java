package com.rohan.aspoc.ui.presenter;

import android.content.Context;

/**
 * Created by rohan on 24/3/16.
 */
public interface MvpView {

    Context getContext();

}
