package com.rohan.aspoc.ui.presenter.impls;

import com.rohan.aspoc.communication.bus.BusProvider;
import com.rohan.aspoc.communication.manager.events.FetchedFeaturedProductsEvent;
import com.rohan.aspoc.communication.manager.events.FetchedProductsEvent;
import com.rohan.aspoc.communication.manager.events.GetFeaturedProductsEvent;
import com.rohan.aspoc.communication.manager.events.GetProductsEvent;
import com.rohan.aspoc.models.Product;
import com.rohan.aspoc.ui.presenter.HomeMvpView;
import com.rohan.aspoc.ui.presenter.Presenter;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;

import java.util.List;

/**
 * Created by rohan on 24/3/16.
 */
public class HomePresenter implements Presenter<HomeMvpView> {

    private HomeMvpView homeMvpView;
    private List<Product> products;
    private Bus mBus = BusProvider.getInstance();
    private List<Product> featuredProducts;

    @Override
    public void attachView(HomeMvpView view) {
        this.homeMvpView = view;
        mBus.register(this);
    }

    @Override
    public void detachView() {
        this.homeMvpView = null;
        mBus.unregister(this);
    }

    public void loadProducts(){
        homeMvpView.showProgressIndicator();
        mBus.post(new GetProductsEvent());
    }

    @Subscribe
    public void onProductsFetched(FetchedProductsEvent event){
        if(!event.isStatus()){
            //failed to fetch set error message or toast it, since no actual api is hit not using this
        }else{
            products = event.getData();
            homeMvpView.showProducts(products);
        }
    }

    public void loadFeaturedProducts() {
        //could have had another progressindicator for featured!
        mBus.post(new GetFeaturedProductsEvent());
    }

    @Subscribe
    public void onFetchedFeaturedProducts(FetchedFeaturedProductsEvent event){
        if(!event.isStatus()){
            //failed to fetch set error message or toast it
        }else{
            featuredProducts = event.getData();
            homeMvpView.showFeaturedProducts(featuredProducts);
        }
    }


    public Product getFeaturedProductAt(int position) {
        return featuredProducts.get(position);
    }

    public List<Product> getProducts() {
        return products;
    }
}
