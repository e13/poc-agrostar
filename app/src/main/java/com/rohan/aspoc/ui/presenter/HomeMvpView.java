package com.rohan.aspoc.ui.presenter;

import com.rohan.aspoc.models.Product;

import java.util.List;

/**
 * Created by rohan on 24/3/16.
 */
public interface HomeMvpView extends MvpView {

    void showProducts(List<Product> products);
    void showFeaturedProducts(List<Product> products);
    void showMessage(int id);
    void showProgressIndicator();
}
