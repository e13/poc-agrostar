package com.rohan.aspoc.utils;

import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.support.v7.app.AlertDialog;

import com.rohan.aspoc.R;
import com.rohan.aspoc.ui.activities.HomeActivity;

import java.util.Locale;

/**
 * Created by rohan on 24/3/16.
 */
public class LanguageUtils {
    private static Context mContext;

    public static void init(Context context) {
        mContext = context;
        showDialogForLocales();
    }

    private static void showDialogForLocales() {
        //should probably extract it out to strings array in resources..
        final String[] locales = {"en", "fr", "ja"};

        new AlertDialog.Builder(mContext)
                .setSingleChoiceItems(locales, 0, null)
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        dialog.dismiss();
                        int selectedPosition = ((AlertDialog) dialog).getListView().getCheckedItemPosition();
                        // Do something useful withe the position of the selected radio button
                        restartInLocale(new Locale(locales[selectedPosition]));
                    }
                })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .show();
    }

    private static void restartInLocale(Locale locale) {
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        Resources resources = mContext.getResources();
        resources.updateConfiguration(config, resources.getDisplayMetrics());
        ((HomeActivity)mContext).recreate();
    }
}
