package com.rohan.aspoc.ui.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.rohan.aspoc.R;
import com.rohan.aspoc.application.Constants;
import com.rohan.aspoc.models.Product;
import com.rohan.aspoc.utils.FontUtils;
import com.squareup.picasso.Picasso;

/**
 * Created by rohan on 24/3/16.
 */
public class ProductDetailsActivity extends BaseActivity {

    private Toolbar mToolbar;
    private ImageView ivProductImage;
    private TextView tvProductName;
    private TextView tvProductDescription;
    private TextView tvProductBrand;
    private TextView tvProductPrice;

    public static Intent newIntent(Context context, Product product) {
        Intent intent = new Intent(context, ProductDetailsActivity.class);
        intent.putExtra(Constants.BUNDLE_KEYS.EXTRA_PRODUCT, product);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_details);

        FontUtils.init(this);
        initViews();
    }

    private void initViews() {
        initToolBar();
        initProductViews();
    }

    private void initProductViews() {
        initPlaceHolders();
        renderProductData();
    }

    private void renderProductData() {
        Product product = getIntent().getParcelableExtra(Constants.BUNDLE_KEYS.EXTRA_PRODUCT);

        Picasso.with(this).load(product.getImageUrl()).placeholder(R.drawable.icon_launcher).into(ivProductImage);
        tvProductName.setText(getString(R.string.details_product_name, product.getName()));
        tvProductDescription.setText(getString(R.string.details_product_description, product.getDescription()));
        tvProductBrand.setText(getString(R.string.details_product_brand, product.getBrandName()));
        tvProductPrice.setText(getString(R.string.details_product_price,product.getPrice()));

        setTitle(product.getName());
    }

    private void initPlaceHolders() {
        ivProductImage = (ImageView) findViewById(R.id.iv_product_image);
        tvProductName = (TextView) findViewById(R.id.tv_product_name);
        tvProductDescription = (TextView) findViewById(R.id.tv_product_description);
        tvProductBrand = (TextView) findViewById(R.id.tv_product_brand);
        tvProductPrice = (TextView) findViewById(R.id.tv_product_price);
    }

    private void initToolBar() {
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
}
