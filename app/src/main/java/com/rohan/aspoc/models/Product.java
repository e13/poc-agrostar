package com.rohan.aspoc.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by rohan on 24/3/16.
 */
public class Product implements Parcelable {
    @SerializedName("product_name")
    private String name;
    @SerializedName("product_description")
    private String description;
    @SerializedName("product_pic")
    private String imageUrl;
    @SerializedName("brand")
    private String brandName;
    @SerializedName("rate")
    private float price;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "Product{" +
                "name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", imageUrl='" + imageUrl + '\'' +
                ", brandName='" + brandName + '\'' +
                ", price='" + price + '\'' +
                '}';
    }


    /**
     * ==============Parcelable START==================
     */

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.name);
        dest.writeString(this.description);
        dest.writeString(this.imageUrl);
        dest.writeString(this.brandName);
        dest.writeFloat(this.price);
    }

    public Product() {
    }

    protected Product(Parcel in) {
        this.name = in.readString();
        this.description = in.readString();
        this.imageUrl = in.readString();
        this.brandName = in.readString();
        this.price = in.readFloat();
    }

    public static final Creator<Product> CREATOR = new Creator<Product>() {
        public Product createFromParcel(Parcel source) {
            return new Product(source);
        }

        public Product[] newArray(int size) {
            return new Product[size];
        }
    };

    /**
     * =====================PARCELABLE END======================
     */



    /**
     * constructor for dummy data
     */
    public Product(String name, String description, String imageUrl, String brandName, float price) {
        this.name = name;
        this.description = description;
        this.imageUrl = imageUrl;
        this.brandName = brandName;
        this.price = price;
    }
}
