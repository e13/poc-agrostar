package com.rohan.aspoc.utils;

import com.rohan.aspoc.models.Product;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by rohan on 24/3/16.
 */
public class MockUtils {
    static String[] imageUrls = new String[]
            {
                    "http://images.boomsbeat.com/data/images/full/19640/game-of-thrones-season-4-jpg.jpg",
                    "https://scontent.fdel1-2.fna.fbcdn.net/hprofile-prn2/v/t1.0-1/c162.37.459.459/s160x160/1016060_10151637219222579_430145459_n.jpg?oh=1c7ad6c79c081d5207fd569348309439&oe=575183DD",
                    "https://lh3.googleusercontent.com/-JTE2wA2yAFA/AAAAAAAAAAI/AAAAAAAAAD8/np30jAgiVyA/s180-c-k-no/photo.jpg",
                    "http://loremflickr.com/320/240/grains/all?random=1",
                    "http://loremflickr.com/320/240/crops,fruits/all?random=1",
            };

    public static List<Product> getProducts() {
        return getDummyProducts(12);
    }

    public static List<Product> getFeaturedProducts() {
        return getDummyFeaturedProducts(6);
    }

    /**
     * private generators
     */

    private static List<Product> getDummyFeaturedProducts(int n) {
        List<Product> products = new ArrayList<Product>();
        for (int i = 0; i < n; i++) {
            products.add(new Product("Name" + i, "Desription" + i, getRandomImage(), "Brand" + i, 100 + i));
        }
        return products;
    }

    private static List<Product> getDummyProducts(int n) {
        List<Product> products = new ArrayList<Product>();
        for (int i = 0; i < n; i++) {
            products.add(new Product("Name" + i, "Desription" + i + getLoremIpsumString(), getRandomImage(), "Brand" + i, 100 + i));
        }
        return products;
    }

    public static String getRandomImage() {
        return imageUrls[new Random().nextInt(imageUrls.length)];
    }

    public static String getLoremIpsumString() {
        return "AgroStar aims to transform Agri-business for farmers in rural India. We have built a “direct to farmer” m-commerce platform through which farmers can procure agri-inputs needed for their farms by simply giving a missed call on our platform and eventually accessing our mobile app.";
    }
}
