package com.rohan.aspoc.application;

import android.app.Application;
import android.content.Context;
import android.graphics.Typeface;

import com.norbsoft.typefacehelper.TypefaceCollection;
import com.norbsoft.typefacehelper.TypefaceHelper;
import com.rohan.aspoc.communication.bus.BusProvider;
import com.rohan.aspoc.communication.manager.EventManager;
import com.squareup.otto.Bus;

/**
 * Created by rohan on 24/3/16.
 */
public class App extends Application {
    private static Context context;

    private EventManager mManager;
    private Bus mBus = BusProvider.getInstance();

    @Override
    public void onCreate() {
        super.onCreate();

        TypefaceCollection typeface = new TypefaceCollection.Builder()
                .set(Typeface.NORMAL, Typeface.createFromAsset(getAssets(), "fonts/gotham/Gotham-Rounded-Book.ttf"))
                .set(Typeface.BOLD, Typeface.createFromAsset(getAssets(), "fonts/gotham/Gotham-Rounded-Bold.ttf"))
                .create();
        TypefaceHelper.init(typeface);

        App.context = getApplicationContext();

        mManager = new EventManager(this, mBus);
        mBus.register(mManager);
        mBus.register(this);
    }

    public static Context getAppContext() {
        return App.context;
    }
}
