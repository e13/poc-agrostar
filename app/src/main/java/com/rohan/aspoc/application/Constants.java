package com.rohan.aspoc.application;

/**
 * Created by rohan on 24/3/16.
 */
public class Constants {
    public static final String PROD_URL = "http://:8090";
    public static final String LOCAL_URL = "http://192.168.1.2:8090";
    public static final String DEV_URL = "http://";

    private static final String API_VERSION = "/api/v1";

    //ACTUAL URL
    public static final String BASE_URL = DEV_URL + API_VERSION;

    public static final int CONNECTION_TIMEOUT = 10;

    //sharedPrefs file
    public static final String PREFERENCES_FILENAME = "asPoC_PREFERENCES_FILE";

    public static final String LINKED_IN = "https://in.linkedin.com/in/rohan90";

    public class BUNDLE_KEYS {
        public static final String EXTRA_PRODUCT = "product";
        public static final String EXTRA_SEARCH_QUERY = "query";
        public static final String EXTRA_PRODUCT_LIST = "productList";
    }

    public class REQUEST_CODES {
    }

    public class RESULT_CODES {
    }

    public class REST_API {
        public static final String BASE = "/products/";
        public static final String GET_PRODUCTS = BASE + "all";
        public static final String GET_FEATURED_PRODUCTS = BASE +"featured";
    }
}
