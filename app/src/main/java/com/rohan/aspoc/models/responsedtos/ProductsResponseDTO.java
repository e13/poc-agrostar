package com.rohan.aspoc.models.responsedtos;

import com.google.gson.annotations.SerializedName;
import com.rohan.aspoc.models.Product;

import java.util.List;

/**
 * Created by rohan on 24/3/16.
 */
public class ProductsResponseDTO {
    @SerializedName("message")
    private String message;
    @SerializedName("status")
    private boolean status;
    @SerializedName("data")
    private List<Product> data;

    public String getMessage() {
        return message;
    }

    public boolean isStatus() {
        return status;
    }

    public List<Product> getData() {
        return data;
    }
}
